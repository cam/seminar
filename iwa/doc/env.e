.uh "Flaming Streams"
.pp
The bloated code produced by the spiral of doubt brought on by the programmer
stereotype not only affects humanity, but also the environment. Code requires
power, and bloated code requires much more than average. This may not seem
like a huge problem on first glance, but that power is all too often obtained
through burning fossil fuels. The programmer stereotype, through the bloated
code produced in the induced spiral of doubt alienation from elitist help
forums such as StackOverflow, quite literally has a direct envirnmental impact.
It is common knowledge that global warming is wreaking havoc on both the
environment and society, but little is being done about it. Obvious targets
for reducing the carbon footprint are being regulated to prevent excess pollution,
but poorly written code has a much greater impact. According to an article by
Microsoft on sustainable programming, the average laptop releases 152kg of
carbon dioxide, a contributer to global warming. This may not seem hugs,
especially compared to the 5200kg of carbon released by the average motor
vehicle, but most families only drive one to two cars, but use 11 to 16 electronic
devices, and replace them much more often. The average American family contributes
200kg more carbon from their smart devices than their cars.
Even worse, the carbon footprint of smart devices is steadily growing, and
poor code is contributing. The sustainable programming movement, despite
gaining powerful allies like Microsoft, has little control over novice
programmers, and novice programmers still write the majority of code run
on devices. The bloated code produced by the programmer stereotype quite
literally has an environmental impact.

