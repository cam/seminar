.uh "Death Spiral"
.pp
The most direct consequence of the programmer stereotype is the mental heath of
programmers. According to a 2020 survey by StackOverflow, an industry leader in
online programming resources, 8.9% of respondants were diagnosed with a
depressive disorder, or about 1.8% higher than the national average, as recorded
by the National Institute of Mental Health in 2017. When considering all mental
disorders, programmers are 12.5% more likely to suffer from a mental disorder
than the average American, as recorded by StackOverflow and the National
Institute of Mental Health, respectively. Small percentages often get overlooked
because of their unimpressive value, but these apply to tens of millions of
people. An experiment comparing the number of people suffering from mental
health issues in a sample of 100,000 programmers and the number of people
suffering from mental health issues in a sample of 100,000 Americans, there were
thousands more programmers affected than average Americans. In software circles,
poor mental health is common enough to get a title: "coding depression". The
programmer stereotype is a major contributor to poor mental health among
programmers because it compounds the stress and loneliness already present in
the lives of many programmers. Programmers are not more likely to suffer from a 
mental disorder because of genetic predisposition, but because of stressors like
the programmer stereotype. However, the programmer stereotype, like most
stereotypes, isn't actually found in reality. The programmer stereotype is a
by-product of cinema and internet cultures, not an idealogy that actually
existed. The spiral of doubt incurred by the stereotype is like depressions
caused by others, but the programmer stereotype often isn't recognized as that,
and spiraling individuals are left to do just that. The popular representation
of a computer programmer is the lonely, often slovenly hacker who types out
three things and ends whatever plotline is relying on that moment. Actual
programming is a neglected subject matter in fiction, so most people synonymize
programmer and hacker. This stereotype is not only detrimental to mental health
for blatant reasons, but also for a slightly more subtle one: the hacker is
almost always depicted with an extreme talent for their craft. Novice
programmers are comparing themselves to an impossible ideal, and spiral into
depression because they can't meet that ideal.
In "The Namesake", Jhumpa Lahiri describes his life given the name of a famous
painter. Like novice programmers, he can't live up to the ideal presented, and
has no ambition to achieve because of that. The novice programmer is disenchanted
by the unatainable media ideal, and spirals into depression, like Lahiri, because
of it.
The programming stereotype drives
programmers into spirals of doubt and depression because they can't meet an
impossible standard, even to the point where statistically, 1.8% more
programmers are depressed than normal persons. The programming stereotype drives
programmers into a state where even their code quality plummets. The programming
stereotype drives programmers to despair over nothing more than an impossible
dream.
