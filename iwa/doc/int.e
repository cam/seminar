.pp
The programmer stereotype pervades modern society, impacting much more than the 
average person realizes. Novice programmers are driven by the stereotype, fueled
by the promises of ease and clean, small source bases, but this is rarely the
case. The programmer stereotype drives these same into deadly spirals, diving
deeper and deeper into a depression fueled by poorly written and bloated code,
with that code fueled by deeper and deeper certainty that the novie cannot
achieve what more advanced programmers can.
Like Jhumpa Lahiri being embarrassed for his given name, novice programmers
are embarrassed of their programming ability and spiral further and further
into doubt.
To make matters worse, advanced
programmers tend to gatekeep their elite status, with even longtime members of
the community being shunned for attempting projects supposedly beyond their
capabilities. The lack of resources provided to novice programmers and the
death spiral associated with attempting complicated projects also contributes
to software bloat which introduces a host of other issues, included economic
destability and environmental destruction. The decreased reliability of
bloatware combined with stable, or supported software never being updated until
the system literally crashes, can cause unpredictable changes in stocks as
companies are temporarily through out by 50-year-old software crashing. Since a
significant percentage of software sold on the market is bloatware, the chances
companies losing stock value increases drastically. Bloatware also has a
sizeable impact on the environment, since more code has to be run in a bloated
application than the corresponding minimalist one. Computers require power to
run, so more code, by definition, needs more power. When combined with the often
nonrenewable sources fueling these power stations, the environmental cost of
bloated software becomes evident. The majority or bloatware is created in the
doubt spiral, reinforced by negative community reactions, and is, at the core,
a direct by-product of the programmer stereotype. The programmer stereotype
directly influences the world we know.

