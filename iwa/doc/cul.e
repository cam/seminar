.uh "Just No"
.pp
Most programmer subcultures are quite welcoming to newcomers, but that
acceptance ends when novices attempt projects beyond their capabilities. Elitism
abounds in programmer subcultures, and all of the negative aspects carry over,
just with the added stress involved with coding and imposiible ideal.
StackOverflow is an industry leader in online resources for programmers via a
question-and-answer system. The communities hosted on the StackExchange network
(the parent company of StackOverflow), are allowed to use their own rules for
what is allowed on the community and the manner in which questions are
formatted, but most communities just keep the defaults. This has caused a large
number of elite StackOverflow users who know the question formula well and are
quite willing to call out anyone who violates the rules. Unfortunately, the
elite StackOverflow users are all too often elitist, and create as negative
atmosphere for novice users of the service. Any deviation from the question
formula, regardless of the question, is almost guarrenteed to get the post
downvoted into oblivion (a downvote marks the post as not meeting the standards
of StackOverflow), and hurts the poster's chances of asking more questions. The
elitism present on StackOverflow prevents novices from asking questions and thus
improving. Unless the elite StackOverflow users deem a question as acceptable,
novice users are forced to rely on often old or unreliable documents regarding
their question. Elitism forms a roadblock for novices and directly contributes
to their stress and spiral of doubt. By effectively telling novices that they
aren't good enough to recieve the information they asked for, elitists
forcefully remind novices that they aren't living up to the programmer
stereotype.
