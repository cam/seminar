.in .5i
.ti 0i
Anonymous. (2015).
.i
Getting up to speed on high frequency trading.
.b
https://www.nasdaq.com/articles/getting-speed-high-frequency-trading-2015-12-04
.r
.ti 0i
Anonymous. (2020).
.i
SAMHSA.
.r
2020 NSDUH.
.b
samhsa.gov/data/release/2020-national-survey-drug-use-and-health-nsduh-releases
.r
.ti 0i
Anonymous. (2021).
.i
StackOverflow.
.r
2021 Developer Survey.
.b
insights.stackoverflow.com/survey/2021
.r
.ti 0i
Khusro, S., Naeem, M., Alam, I. (2018).
.i
Journal of Information Communication Technologies and Robotic
Applications, 20-30.
.r
There is no such thing as a free lunch: an investigation of
bloatware effects on smart devices.
.b
pISSN: 2523-5729.
.r
.ti 0i
Lahiri, J. (2003).
.i
Chapter 4. In The Namesake.
.b
Essay, Mariner Books, Houghton Mifflin Harcourt.
.r
.ti 0i
Manne, S. (2020).
.i
Examining the carbon footprint of devices.
.b
https://devblogs.microsoft.com/sustainable-software/examining-the-carbon-footprint-of-devices/
.r
.ti 0i
Yagemann, C., Chung, S., Uzun, E., Ragam, S., Saltaformaggio, B., Lee, W.
(2020).
.i
ACSAC '20.
.r
On the feasability of automating stock market manipulation.
.b
doi: 10.1145/3427228.3427241
