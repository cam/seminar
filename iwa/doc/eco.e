.uh "Busting Seams"
.pp
The global stock exchange drives the modern market, whether through the millions
of companies supported by investors or the individual just trying to make free
money. Contrary to popular belief, the code executed at blistering speeds in
high-speed-trading (HST) isn't as efficient as it could be. Bloated software directly
influences economic trends through HST software and web
technologies, and the programmer stereotype only exacerbates the negative
effects. According to an article by the Nasdaq commission, HST is dictated mostly
by latency, or the time a transaction takes to complete. 
The latency produced by poorly written code regularly causes stock prices to vary
much more than investors would like, but this variation is all too often chalked
up to distance (another common, but unavoidable, form of latency). Instead of
addressing the core issue, investors are choosing to allow poorly written code
to decide the fate of the economy. The programmer stereotype, though the spiral
of doubt created via elitism help forums and a toxic media image, causes the
market to be at a much greater risk of collapse.

